# MACARONI A CPP VERSION OF MACARENA.

<img src="icon/macaroni++.png"
     alt="Macaroni icon"
     style="float: left; margin-right: 10px;" />

A BASIC LIBRARY FOR BASIC FUNCTIONS

 **Copyright (c) 2020 by Andrés Galván**
     
 Macaroni 0.0.1 - Basic functions to make console programs faster.
                      
 Macaroni is a student usage oriented micro-library, that is a C++
 version of **[Macarena Module](https://github.com/JuniorWriter/Macarena)** made for python. This simple Library is
 for basic side functions that allow the developer (student) to 
 focus on the logic of the main process.


# HOW TO SETUP.

To setup the library in your machine, you have to clone or download the repository copy the src folder in your project or your task and then call the library by:

 `#Include "<relative location>/src/macaroni.hh".`

Now you can use the functions that the library brings.


# USAGE.

This library is moderatly compatible with both *Unix-like* and *Windows* Operative Systems,
so, you don't have to worry about compibilities functionalities

### Basic functions.
* **clearConsole**()
    This function clean your console whichever is


* **errorMsg**(int errorType);
    This function print the common error types
    in red fore color with the same format and
    with a error code.
    * error list:
        * "Error List Working.
        * "Error 101. Solo se permiten numeros.
        * "Error 102. Solo se permiten numeros positivos.
        * "Error 103. Opción fuera de rango.", //3
        * "Error 104. Nombre muy corto.", //4
        * "Error 105. Solo ingrese numeros negativos.
        * "Error 106. Sólo se permiten letras.
        * "Error 107. Debes ingresar algo.
        * "Error 108. El limite superior debe ser mayor.
        * "Macaroni Syntax error. In function's parameter.


* **errorMsgPersonalized**(std::string message);
* **errorIdentifier**(std::string errorID);
* **printTitle**(std::string title, int centered, bool inCaps);
* **isInteger**(std::string evaluate);
* **isFloat**(std::string evaluate);
* **removeSpaces**(char *str);
* **repeatProgram****(int main_function());
* **inputName**(char *name);
* **inputNumbers**(char numbersRange, std::string evaluate);

